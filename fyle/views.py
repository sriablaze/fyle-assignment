from __future__ import unicode_literals

import json

from django.http import HttpResponse


def render_description(request):
    branch_ifsc_api = {
        'url': '/get/detail',
        'arguments': 'ifsc_code',
        'example': 'https://enigmatic-dawn-64578.herokuapp.com/api/v1/branch/get/detail/?ifsc=ABHY0065001',
        'method': 'GET'
    }
    branch_list_citywise_api = {
        'url': '/filter/city-wise',
        'arguments': 'city_name, bank_name',
        'example': 'https://enigmatic-dawn-64578.herokuapp.com/api/v1/branch/filter/city-wise/?city=delhi&bank=ALLAHABAD%20BANK',
        'method': 'GET'
    }
    branch_api = {
        'base_endpoint': '/api/v1/branch',
    }
    branch_api['get specific branch given an ifsc'] = branch_ifsc_api
    branch_api['get branches of a bank in the city'] = branch_list_citywise_api
    return HttpResponse(json.dumps(branch_api), status=200)
