from tastypie.api import Api

from django.conf.urls import include
from django.conf.urls import url

from fyle.bank_api import BankResource
from fyle.bank_api import BranchResource
from fyle.views import render_description

v1_api = Api(api_name='v1')
v1_api.register(BankResource())
v1_api.register(BranchResource())

urlpatterns = [
    url(r'^api/', include(v1_api.urls)),
    url(r'', render_description, name='render_description')
]
