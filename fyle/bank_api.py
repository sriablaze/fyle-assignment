from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.http import HttpBadRequest
from tastypie.http import HttpNotFound
from tastypie.http import HttpUnauthorized
from tastypie.resources import ModelResource
from tastypie.utils import trailing_slash

from django.conf.urls import url

from fyle import constants
from fyle import utils
from fyle.models import Bank
from fyle.models import Branch


class BankResource(ModelResource):
    class Meta:
        queryset = Bank.objects.all()
        resource_name = 'bank'
        include_resource_uri = False
        excludes = ['id']


class BranchResource(ModelResource):
    bank = fields.ToOneField(
        'fyle.bank_api.BankResource',
        'bank',
        full=True
    )
    class Meta:
        queryset = Branch.objects.all()
        resource_name = 'branch'
        include_resource_uri = False

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get/detail%s$" %
            (self._meta.resource_name, trailing_slash()),
            self.wrap_view('get_branch_details'),
            name="api_get_branch_details"),
            url(r"^(?P<resource_name>%s)/filter/city-wise%s$" %
            (self._meta.resource_name, trailing_slash()),
            self.wrap_view('get_citywise_list'),
            name="api_get_citywise_list"),
        ]

    def get_branch_details(self, request, **kwargs):
        ifsc_code = request.GET.get('ifsc')
        if not ifsc_code:
            return self.error_response(
                request,
                utils.get_error_response(
                    'BadRequest',
                    'IFSC code not present in request'
                ),
                HttpBadRequest
            )
        try:
            branch = Branch.objects.get(ifsc=ifsc_code)
            branch_bundle = self.build_bundle(request=request, obj=branch)
            dehydrated_bundle = self.full_dehydrate(branch_bundle)
            return self.create_response(request, dehydrated_bundle)
        except Branch.DoesNotExist:
            return self.error_response(
                request,
                utils.get_error_response(
                    'DoesNotExist',
                    'Branch does not exist'
                ),
                HttpBadRequest
            )


    def get_citywise_list(self, request, **kwargs):
        city = request.GET.get('city')
        bank_name = request.GET.get('bank')
        if not city or not bank_name:
            return self.error_response(
                request,
                utils.get_error_response(
                    'BadRequest',
                    'Bank name or city name not present in request'
                ),
                HttpBadRequest
            )

        branches = Branch.objects.filter(city__iexact=city, bank__name__iexact=bank_name)
        bundles = []
        for branch in branches:
            bundle = self.build_bundle(obj=branch, request=request)
            bundles.append(self.full_dehydrate(bundle, for_list=True))
        return self.create_response(request, bundles)
