# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Bank(models.Model):
    unique_id = models.IntegerField(unique=True)
    name = models.CharField(max_length=50)

    def __str__(self):
        return str(self.unique_id)


class Branch(models.Model):
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE)
    ifsc = models.CharField(max_length=20)
    branch_code = models.CharField(max_length=50)
    address = models.CharField(max_length=500)
    city = models.CharField(max_length=25)
    district = models.CharField(max_length=25)
    state = models.CharField(max_length=25)

    def __str__(self):
        return self.ifsc
