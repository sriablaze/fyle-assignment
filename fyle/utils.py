import csv
import os

from fyle import constants
from fyle.models import Bank
from fyle.models import Branch

dirpath = os.getcwd()

def add_bank_data_to_db():
    file = open(os.path.abspath("{0}/fyle/bank_branches.csv".format(dirpath)), "r")
    reader = csv.reader(file)
    for index, line in enumerate(reader):
        if index == 0:
            continue
        stripped_row = [column_value.strip() for column_value in line]
        ifsc = stripped_row[0]
        bank_id = int(stripped_row[1])
        branch_code = stripped_row[2]
        address = stripped_row[3]
        city = stripped_row[4]
        district = stripped_row[5]
        state = stripped_row[6]
        bank_name = stripped_row[7]
        bank, created = Bank.objects.get_or_create(unique_id=bank_id,
            defaults={'name':bank_name, 'unique_id': bank_id}
        )
        Branch.objects.create(
            bank=bank,
            ifsc=ifsc,
            branch_code=branch_code,
            address=address,
            city=city,
            district=district,
            state=state,
        )


def get_error_response(error_code, error_message):
    return {
        constants.API_ERROR_CODE: error_code,
        constants.API_ERROR_MESSAGE: error_message
    }
